<?php

namespace Chill\CustomFieldsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;
use Chill\CustomFieldsBundle\Form\DataTransformer\CustomFieldsGroupToIdTransformer;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelper;


class CustomFieldType extends AbstractType
{
    /**
     *
     * @var CustomFieldProvider
     */
    private $customFieldProvider;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    private $translatableStringHelper;

    /**
     * @var ObjectManager
     */
    private $om;


    public function __construct(
        CustomFieldProvider $compiler,
        ObjectManager $om,
        TranslatableStringHelper $translatableStringHelper
    ) {
        $this->customFieldProvider = $compiler;
        $this->om = $om;
        $this->translatableStringHelper = $translatableStringHelper;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $customFieldsList = array();

        foreach ($this->customFieldProvider->getAllFields() as $key => $field) {
            $customFieldsList[$key] = $field->getName();
        }

        $builder
            ->add('name', TranslatableStringFormType::class)
            ->add('active', CheckboxType::class, array('required' => false));

        if ($options['group_widget'] === 'entity') {
            $builder->add('customFieldsGroup', EntityType::class, array(
               'class' => 'ChillCustomFieldsBundle:CustomFieldsGroup',
               'choice_label' => function($g) {
                    return $this->translatableStringHelper->localize($g->getName());
               }
            ));
        } elseif ($options['group_widget'] === 'hidden') {
            $builder->add('customFieldsGroup', HiddenType::class);
            $builder->get('customFieldsGroup')
                  ->addViewTransformer(new CustomFieldsGroupToIdTransformer($this->om));
        } else {
            throw new \LogicException('The value of group_widget is not handled');
        }

        $builder
            ->add('ordering', NumberType::class)
            ->add('required', CheckboxType::class, array(
                'required' => false,
                //'expanded' => TRUE,
                'label' => 'Required field'
            ))
            ->add('type', HiddenType::class, array('data' => $options['type']))
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event)
            {
                $customField = $event->getData();
                $form = $event->getForm();

                // check if the customField object is "new"
                // If no data is passed to the form, the data is "null".
                // This should be considered a new "customField"
                if (!$customField || null === $customField->getId()) {
                    $form->add('slug', TextType::class);
                }
            });


        $builder->add(
            $this->customFieldProvider
                ->getCustomFieldByType($options['type'])
                ->buildOptionsForm(
                    $builder
                         ->create('options', null, array('compound' => true))
                         ->setRequired(false)
                    )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\CustomFieldsBundle\Entity\CustomField'
        ));

        $resolver->setRequired(array('type', 'group_widget'))
              ->addAllowedValues('type', array_keys($this->customFieldProvider->getAllFields()))
              ->addAllowedValues('group_widget', array('hidden', 'entity'))
              ->setDefault('group_widget', 'entity');
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'custom_field_choice';
    }
}
