<?php
namespace Chill\CustomFieldsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

/**
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 *
 */
class ChoicesType extends AbstractType
{
    public function getBlockPrefix()
    {
        return 'cf_choices';
    }

    public function getParent()
    {
        return CollectionType::class;
    }
}
