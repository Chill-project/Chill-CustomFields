<?php
namespace Chill\CustomFieldsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Chill\MainBundle\Form\Type\TranslatableStringFormType;

class ChoicesListType extends AbstractType
{

    /* (non-PHPdoc)
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TranslatableStringFormType::class)
            ->add('active', CheckboxType::class, array(
                'required' => false
            ))
            ->add('slug', HiddenType::class)
            ->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();

                $formData = $form->getData();

                if (NULL === $formData['slug']) {
                    $slug = uniqid(rand(), true);

                    $data['slug'] = $slug;
                    $event->setData($data);
                } else {
                    $data['slug'] = $formData['slug'];
                    $event->setData($data);
                }
            })
        ;
    }


    /*
     *
     * @see \Symfony\Component\Form\FormTypeInterface::getName()
     */
    public function getBlockPrefix()
    {
        return 'cf_choices_list';
    }

}
