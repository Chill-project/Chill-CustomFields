<?php
namespace Chill\CustomFieldsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Return a choice widget with an "other" option
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 *
 */
class ChoiceWithOtherType extends AbstractType
{
    private $otherValueLabel = 'Other value';

    /* (non-PHPdoc)
     * @see \Symfony\Component\Form\AbstractType::buildForm()
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        //add an 'other' entry in choices array
        $options['choices'][$this->otherValueLabel] = '_other';
        //ChoiceWithOther must always be expanded
        $options['expanded'] = true;
        // adding a default value for choice
        $options['empty_data'] = null;
        $options['choices_as_values'] = true;

        $builder
            ->add('_other', TextType::class, array('required' => false))
            ->add('_choices', ChoiceType::class, $options)
        ;
    }

    /* (non-PHPdoc)
     * @see \Symfony\Component\Form\AbstractType::configureOptions()
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired(array('choices'))
            ->setAllowedTypes('choices', array('array'))
            ->setDefaults(array(
               'multiple' => false
               ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'choice_with_other';
    }
}
