<?php

/*
 * Chill is a software for social workers
 * Copyright (C) 2015 Champs Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\CustomFields\Tests\Templating\Twig;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\CustomFieldsBundle\Templating\Twig\CustomFieldRenderingTwig;
use Chill\CustomFieldsBundle\Entity\CustomField;
use Chill\CustomFieldsBundle\Service\CustomFieldProvider;

/**
 * Test the rendering of twig function which renders custom fields
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class CustomFieldRenderingTwigTest extends KernelTestCase
{
    /**
     *
     * @var CustomFieldRenderingTwig
     */
    private $cfRendering;
    
    /**
     *
     * @var CustomFieldProvider
     */
    private $cfProvider;
    
    public function setUp()
    {
        self::bootKernel();
        $this->cfRendering = self::$kernel->getContainer()
              ->get('chill.custom_field.twig.custom_fields_rendering')
              ;
        
        $this->cfProvider = self::$kernel->getContainer()
              ->get('chill.custom_field.provider');
        
        // set locale to fr
        $prophet = new \Prophecy\Prophet;
        $request = $prophet->prophesize();
        $request->willExtend('Symfony\Component\HttpFoundation\Request');
        $request->getLocale()->willReturn('fr');
        self::$kernel->getContainer()->get('request_stack')
              ->push($request->reveal());
    }
    
    /**
     * 
     * @return CustomField
     */
    private function getSimpleCustomFieldText()
    {
        return (new CustomField())
            ->setSlug('test')
              ->setName(array('fr' => 'Test'))
              ->setType('text')
              ->setOrdering(10)
              ->setOptions(array("maxLength" => 255))
              ->setActive(true)
              ;
    }
    
    public function testLabelRendering()
    {
        $cf = $this->getSimpleCustomFieldText();
        
        $text = $this->cfRendering->renderLabel($cf);
        
        $this->assertContains('Test', $text,
              "The rendering text should contains the 'test' text");
    }
    
    public function testWidgetRendering()
    {
        $cf = $this->getSimpleCustomFieldText();
        $fields = array(
           'test' => "My tailor is rich"
        );
        
        $text = $this->cfRendering->renderWidget($fields, $cf);
        
        $this->assertContains('My tailor is rich', $text,
              "The rendering text should contains the 'test' text");
    }
    
    public function testIsEmpty()
    {
        $cf = $this->getSimpleCustomFieldText();
        
        // value is not empty
        $fields = array(
           'test' => "My tailor is rich"
        );
        
        $result = $this->cfRendering->isEmptyValue($fields, $cf);
        
        $this->assertFalse($result);
        
        // value is empty
        $fields = array(
           'text' => ''
        );
        
        $result = $this->cfRendering->isEmptyValue($fields, $cf);
        
        $this->assertTrue($result);
    }
}
